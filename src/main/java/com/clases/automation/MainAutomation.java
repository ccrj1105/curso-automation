package com.clases.automation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;


public class MainAutomation {

    public void waitForElement(RemoteWebDriver driver, By element, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(
                ExpectedConditions.or(
                        ExpectedConditions.elementToBeClickable(element),
                        ExpectedConditions.presenceOfElementLocated(element),
                        ExpectedConditions.visibilityOfElementLocated(element),
                        ExpectedConditions.elementToBeSelected(element)));
    }

    public static void main(String[] args) {


        MainAutomation dr = new MainAutomation(); // instancia de mi clase
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        chromeOptions.addArguments("--start-maximized");
        WebDriverManager.chromedriver().setup();// para buscar el driver en mi repo .m2
        RemoteWebDriver driver = new ChromeDriver(chromeOptions);// abrir el navegador
        driver.get("https://www.grupobancolombia.com/personas/productos-servicios/canales-servicio/sucursal-web/sucursal-virtual-personas");
        driver.findElement(By.xpath("//*[@title='Ingresa aquí']")).click();
        List<String> tabHandles = new ArrayList<>(driver.getWindowHandles());
        System.out.println("#ventanas : " + tabHandles.size());
        driver.switchTo().window(tabHandles.get(1));
        dr.waitForElement(driver,By.id("username"),30);
        driver.findElement(By.id("username")).sendKeys("pepito");

        driver.findElement(By.id("btnGo")).click();
        driver.findElement(By.xpath("//div[contains(@id,'8')]")).click();


    }
}
