package com.clases.automation.pom;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;


public class Driver {
    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();


    public RemoteWebDriver getDriver() {

        return driver.get();
    }

@BeforeTest
    public void initDriver() {
        String navegador = "Chrome";
        switch (navegador) {
            case "Chrome":
                WebDriverManager.chromedriver().setup();// para buscar el driver en mi repo .m2
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
                chromeOptions.addArguments("--start-maximized");
                driver.set(new ChromeDriver(chromeOptions));
                break;
            case "firefox":
                break;
        }


    }
}
