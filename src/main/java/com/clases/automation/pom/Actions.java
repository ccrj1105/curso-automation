package com.clases.automation.pom;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Actions {
    private RemoteWebDriver driver;

    Actions(RemoteWebDriver driver) {
        this.driver = driver;

    }

    public WebElement searchElement(By by) {
        return driver.findElement(by);
    }

    public void write(By by, String text) {
        searchElement(by).sendKeys(text);
    }

    public void click(By by) {
        searchElement(by).click();
    }

    public String getTextElement(By by) {
        return searchElement(by).getText();
    }

    public void openPage(String url) {
        driver.get(url);
    }

    public void waitForElement(By element, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(
                ExpectedConditions.or(
                        ExpectedConditions.elementToBeClickable(element),
                        ExpectedConditions.presenceOfElementLocated(element),
                        ExpectedConditions.visibilityOfElementLocated(element),
                        ExpectedConditions.elementToBeSelected(element)));
    }

    public void screenCapture() {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH-mm-ss");
            Date date = new Date();
            String nombreImg = dateFormat.format(date);
            File screen = driver.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screen, new File("img/pantalla" + nombreImg + ".png"));
        } catch (Exception e) {
            System.out.println("imagen no capturada");
            System.out.println(e.getMessage());
        }

    }
}
