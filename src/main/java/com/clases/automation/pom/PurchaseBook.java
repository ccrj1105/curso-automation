package com.clases.automation.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class PurchaseBook {
    Actions actions;
    PurchaseBook(RemoteWebDriver driver){
        this.actions = new Actions(driver);

    }
    private final By btnAdd = By.xpath("//*[@value='Add']");
    private final By txtJavaBook = By.xpath("//*[@id=\"listing\"]/tbody/tr[2]/td[4]/input");
    private final By txtRubyBook = By.xpath("//*[@id=\"listing\"]/tbody/tr[3]/td[4]/input");
    private final By txtPythonBook = By.xpath("//*[@id=\"listing\"]/tbody/tr[4]/td[4]/input");

    public void addBook(String cant1,String cant2,String cant3){
        actions.screenCapture();
        actions.waitForElement(btnAdd,30);
        actions.write(txtJavaBook,cant1);
        actions.write(txtRubyBook,cant2);
        actions.write(txtPythonBook,cant3);
        actions.screenCapture();
        actions.click(btnAdd);
        actions.screenCapture();
    }
}
