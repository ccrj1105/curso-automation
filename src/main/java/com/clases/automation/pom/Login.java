package com.clases.automation.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Login {
    private Actions actions;

    Login(RemoteWebDriver driver) {
        this.actions = new Actions(driver);
    }

    private final By txtUser = By.name("user");
    private final By txtPassword = By.name("password");
    private final By btnLongin = By.className("button");
    private final By messageError = By.id("errorMessage");


    public void login(String user, String pwd) {
        actions.openPage("http://sahitest.com/demo/training/login.htm");
        actions.write(txtUser, user);
        actions.write(txtPassword, pwd);
        actions.click(btnLongin);


    }


    public void loginBad(String user, String pwd) {
        login(user, pwd);
        actions.getTextElement(messageError);
        actions.screenCapture();


    }


}
