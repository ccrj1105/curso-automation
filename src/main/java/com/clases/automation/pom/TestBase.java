package com.clases.automation.pom;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestBase {

    Login login;
    PurchaseBook purchaseBook;

    @BeforeTest
    public void InitDriver() {
        Driver driver = new Driver();
        driver.initDriver();
        login = new Login(new Driver().getDriver());
      purchaseBook   = new PurchaseBook(new Driver().getDriver());

    }

    @Test
    public void testLogin() {

        login.login("test", "secret");
        purchaseBook.addBook("1","5","6");

    }

    @Test
    public void testLoginBad() {
        login.loginBad("test", "u");

    }
}
