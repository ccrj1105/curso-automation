package com.clases.automation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Frame {
    public void waitForElement(RemoteWebDriver driver, By element, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(
                ExpectedConditions.or(
                        ExpectedConditions.elementToBeClickable(element),
                        ExpectedConditions.presenceOfElementLocated(element),
                        ExpectedConditions.visibilityOfElementLocated(element),
                        ExpectedConditions.elementToBeSelected(element)));
    }
    public static void main(String[] args) {
        Frame fm = new Frame();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        chromeOptions.addArguments("--start-maximized");
        WebDriverManager.chromedriver().setup();// para buscar el driver en mi repo .m2
        RemoteWebDriver driver = new ChromeDriver(chromeOptions);// abrir el navegador
        driver.get("https://www.oracle.com/index.html");
        driver.switchTo().frame(driver.findElement(By.xpath("//*[@title='TrustArc Cookie Consent Manager']")));
        fm.waitForElement(driver,By.xpath("//a[@class='call']"),30);
        driver.findElement(By.xpath("//a[@class='call']"));
        driver.switchTo().defaultContent();
    }
}
